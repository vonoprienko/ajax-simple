////Асинхронный XMLHttpRequest
// с таймером при длительном молчании

function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

var xmlhttp = getXmlHttp()
xmlhttp.open("POST", "script2html.txt", true);

xmlhttp.onreadystatechange=function(){
    if (xmlhttp.readyState != 4) return

    clearTimeout(timeout) // очистить таймаут при наступлении readyState 4

    if (xmlhttp.status == 200) {
        // Все ок
    //...
        alert(xmlhttp.responseText);
    //...
    } else {
        handleError(xmlhttp.statusText) // вызвать обработчик ошибки с текстом ответа
    }
}

xmlhttp.send("a=5&b=4");
// Таймаут 10 секунд
var timeout = setTimeout( function(){ xmlhttp.abort(); handleError("Time over") }, 10000);

function handleError(message) {
    // обработчик ошибки
    alert("Ошибка: "+message)
}